import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter/material.dart';
import 'package:project/food_Vocab.dart';

class FoodInfo extends StatefulWidget {
  FoodInfo({Key? key}) : super(key: key);

  @override
  _FoodInfoState createState() => _FoodInfoState();
}

class _FoodInfoState extends State<FoodInfo> {
  String VThai = "";
  String Vocabulary = "";
  int foodId = 0;
  bool isClick = true;
  Icon icons = Icon(
    Icons.star,
    color: Colors.grey,
  );

  final Stream<QuerySnapshot> _foodStream =
      FirebaseFirestore.instance.collection('Food').snapshots();
  CollectionReference food = FirebaseFirestore.instance.collection('Food');

  CollectionReference favorite =
      FirebaseFirestore.instance.collection('favorite');
  Future<void> addFavorite() {
    return favorite
        .add({
          'id': this.foodId,
          'thai': this.VThai,
          'vocabulary': this.Vocabulary,
        })
        .then((value) => print('Favorite Add'))
        .catchError((error) => print("Failed to add favorite: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _foodStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text('Loading');
        }
        return Scaffold(
          appBar: AppBar(
            title: Text('Food'),
          ),
          body: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [Colors.grey.shade900, Colors.grey.shade800])),
              padding: EdgeInsets.all(16.0),
              child: ListView(
                children: snapshot.data!.docs.map((DocumentSnapshot document) {
                  Map<String, dynamic> data =
                      document.data()! as Map<String, dynamic>;
                  return Card(
                    color: Colors.grey.shade700,
                    child: ListTile(
                      title: Text(data['thai']),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                            onPressed: () {
                              setState(() {
                                foodId = data['id'];
                                VThai = data['thai'];
                                Vocabulary = data['vocabulary'];

                                addFavorite();
                              });
                            },
                            icon: icons,
                          )
                        ],
                      ),
                      onTap: () {
                        foodId = data['id'];
                        VThai = data['thai'];
                        Vocabulary = data['vocabulary'];
                        Navigator.push(context, MaterialPageRoute(builder: (context) => FoodVocab(Fid: foodId,Vthai: VThai,Vocab: Vocabulary,)));
                      },
                    ),
                  );
                }).toList(),
              )),
        );
      },
    );
  }
}
