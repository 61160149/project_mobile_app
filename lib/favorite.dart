import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:project/favorite_Vocab.dart';

class FavoriteWidget extends StatefulWidget {
  FavoriteWidget({Key? key}) : super(key: key);

  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  String VThai = "";
  String Vocabulary = "";
  int fvId = 0;
  CollectionReference favorite =
      FirebaseFirestore.instance.collection('favorite');
  final Stream<QuerySnapshot> _favoriteStream =
      FirebaseFirestore.instance.collection('favorite').snapshots();

  Future<void> delFavorite(fvId) {
    return favorite
        .doc(fvId)
        .delete()
        .then((value) => print('User Delete'))
        .catchError((error) => print('Failed to delete user: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _favoriteStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text('Loading');
        }
        return Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.grey.shade900, Colors.grey.shade800])),
            padding: EdgeInsets.all(16.0),
            child: ListView(
              children: snapshot.data!.docs.map((DocumentSnapshot document) {
                Map<String, dynamic> data =
                    document.data()! as Map<String, dynamic>;
                return Card(
                  color: Colors.grey.shade700,
                  child: ListTile(
                    title: Text(data['thai']),
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        IconButton(
                            onPressed: () {
                              setState(() {
                                delFavorite(document.id);
                              });
                            },
                            icon: Icon(Icons.delete)),
                      ],
                    ),
                    onTap: () {
                      fvId = data['id'];
                      VThai = data['thai'];
                      Vocabulary = data['vocabulary'];

                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => FavoriteVocab(
                                  Faid: fvId,
                                  Vthai: VThai,
                                  Vocab: Vocabulary)));
                    },
                  ),
                );
              }).toList(),
            ));
      },
    );
  }
}
