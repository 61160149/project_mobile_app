import 'package:flutter/material.dart';

class ShopVocab extends StatelessWidget {
  int Sid;
  String Vthai;
  String Vocab;

  ShopVocab(
      {Key? key, required this.Sid, required this.Vthai, required this.Vocab})
      : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Shopping'),
      ),
      body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Colors.grey.shade900, Colors.grey.shade800])),
          padding: EdgeInsets.all(16.0),
          child: ListView(children: [
            Card(
              color: Colors.grey.shade700,
              child: ListTile(
                title: Text(
                  '$Vthai',
                  style: TextStyle(fontSize: 17),
                ),
                subtitle: Text(
                  '$Vocab',
                  style: TextStyle(fontSize: 16),
                ),
                onTap: () {
                  
                },
              ),
            )
          ])),
    );
  }
}