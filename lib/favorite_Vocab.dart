import 'package:flutter/material.dart';

class FavoriteVocab extends StatelessWidget {
  int Faid;
  String Vthai;
  String Vocab;

  FavoriteVocab(
      {Key? key, required this.Faid, required this.Vthai, required this.Vocab})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Favorite'),
      ),
      body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Colors.grey.shade900, Colors.grey.shade800])),
          padding: EdgeInsets.all(16.0),
          child: ListView(children: [
            Card(
              color: Colors.grey.shade700,
              child: ListTile(
                title: Text(
                  '$Vthai',
                  style: TextStyle(fontSize: 17),
                ),
                subtitle: Text(
                  '$Vocab',
                  style: TextStyle(fontSize: 16),
                ),
                onTap: () {},
              ),
            )
          ])),
    );
  }
}
