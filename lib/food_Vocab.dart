
import 'package:flutter/material.dart';

class FoodVocab extends StatelessWidget {
  int Fid;
  String Vthai;
  String Vocab;

  FoodVocab(
      {Key? key, required this.Fid, required this.Vthai, required this.Vocab})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Food'),
      ),
      body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Colors.grey.shade900, Colors.grey.shade800])),
          padding: EdgeInsets.all(16.0),
          child: ListView(children: [
            Card(
              color: Colors.grey.shade700,
              child: ListTile(
                title: Text('$Vthai',style: TextStyle(fontSize: 17),),
                subtitle: Text('$Vocab',style: TextStyle(fontSize: 16),),
                
               
              ),
            )
          ])),
    );
  }
}
