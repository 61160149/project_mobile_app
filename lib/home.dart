import 'package:flutter/material.dart';

import 'package:project/food_info.dart';

import 'package:project/genaral_info.dart';

import 'package:project/love_info.dart';

import 'package:project/shopping_info.dart';

class AppBarHome extends StatelessWidget {
  const AppBarHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          child: Column(
            children: <Widget>[
              Text('ENGLISH',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 50,color: Colors.white))
            ],
          ),
          padding: EdgeInsets.all(16.0),
        )
      ],
    );
  }
}

class HomeWidget extends StatefulWidget {
  HomeWidget({Key? key}) : super(key: key);

  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(100.0),
          child:Container(decoration: BoxDecoration(gradient: LinearGradient(colors: <Color>[Colors.grey.shade800,Colors.grey.shade900])),child: AppBarHome(),)
          
          ),
      body: Container(
          decoration: BoxDecoration(gradient: LinearGradient(colors: [Colors.grey.shade900,Colors.grey.shade800])),
          child: Column(
            children: <Widget>[
              Expanded(
                child: ListView(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Column(
                        children: [
                          //Img Food
                          InkWell(
                            onTap: () {
                              Navigator.push(context, MaterialPageRoute(builder: (context) => FoodInfo()));
                            },
                            child: Card(
                              color: Colors.black,
                              shape: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 2)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.all(32.0),
                                    child: Image(
                                      image: AssetImage('images/food.jpg'),
                                      width: 300,
                                      height: 300,
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 32.0),
                                    child: Text(
                                      'Food',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 50,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Row(
                            children: [
                              Expanded(
                                //image Teacher
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => GenaralInfo()));
                                  },
                                  child: Card(
                                    color: Colors.black,
                                    shape: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(20),
                                        borderSide: BorderSide(
                                            color: Colors.grey, width: 2)),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.all(32.0),
                                          child: Image(
                                            image: AssetImage('images/teach.jpg'),
                                          ),
                                        ),
                                        Padding(
                                    padding: EdgeInsets.only(bottom: 32.0),
                                    child: Text(
                                      'Genaral',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 50,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                //Image Love
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => LoveInfo()));
                                  },
                                  child: Card(
                                    color: Colors.black,
                                    shape: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(20),
                                        borderSide: BorderSide(
                                            color: Colors.grey, width: 2)),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.all(32.0),
                                          child: Image(
                                            image: AssetImage('images/love.jpg'),
                                          ),
                                        ),
                                        Padding(
                                    padding: EdgeInsets.only(bottom: 32.0),
                                    child: Text(
                                      'Love',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 50,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.push(context, MaterialPageRoute(builder: (context) => ShoppingInfo()));
                            },
                            child: Card(
                              color: Colors.black,
                              shape: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 2)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.all(32.0),
                                    child: Image(
                                      image: AssetImage('images/shopping.jpg'),
                                      width: 300,
                                      height: 300,
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 32.0),
                                    child: Text(
                                      'Shopping',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 50,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              
            ],
            
          )),
    );
  }
}
