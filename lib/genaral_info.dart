import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:project/genaral_Vocab.dart';
import 'package:project/main.dart';

class GenaralInfo extends StatefulWidget {
  GenaralInfo({Key? key}) : super(key: key);

  @override
  _GenaralInfoState createState() => _GenaralInfoState();
}

class _GenaralInfoState extends State<GenaralInfo> {
  final Stream<QuerySnapshot> _genaralStream =
      FirebaseFirestore.instance.collection('genaral').snapshots();
  CollectionReference users = FirebaseFirestore.instance.collection('genaral');
  String VThai = "";
  String Vocabulary = "";
  int genId = 0;
  CollectionReference favorite =
      FirebaseFirestore.instance.collection('favorite');
  Future<void> addFavorite() {
    return favorite
        .add({
          'id': this.genId,
          'thai': this.VThai,
          'vocabulary': this.Vocabulary,
        })
        .then((value) => print('Favorite Add'))
        .catchError((error) => print("Failed to add favorite: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _genaralStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text('Loading');
        }
        return Scaffold(
          appBar: AppBar(
            title: Text('Genaral'),
          ),
          body: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [Colors.grey.shade900, Colors.grey.shade800])),
              padding: EdgeInsets.all(16.0),
              child: ListView(
                children: snapshot.data!.docs.map((DocumentSnapshot document) {
                  Map<String, dynamic> data =
                      document.data()! as Map<String, dynamic>;
                  return Card(
                    color: Colors.grey.shade700,
                    child: ListTile(
                      title: Text(data['thai']),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                              onPressed: () {
                                setState(() {
                                  genId = data['id'];
                                  VThai = data['thai'];
                                  Vocabulary = data['vocabulary'];

                                  addFavorite();
                                });
                              },
                              icon: Icon(Icons.star)),
                        ],
                      ),
                      onTap: () {
                        genId = data['id'];
                        VThai = data['thai'];
                        Vocabulary = data['vocabulary'];
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => GenaralVocab(
                                      Gid: genId,
                                      Vthai: VThai,
                                      Vocab: Vocabulary,
                                    )));
                      },
                    ),
                  );
                }).toList(),
              )),
        );
      },
    );
  }
}
