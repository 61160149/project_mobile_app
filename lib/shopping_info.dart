import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:project/favorite.dart';
import 'package:project/favorite_Vocab.dart';
import 'package:project/shopping_Vocab.dart';

class ShoppingInfo extends StatefulWidget {
  ShoppingInfo({Key? key}) : super(key: key);

  @override
  _ShoppingInfoState createState() => _ShoppingInfoState();
}

class _ShoppingInfoState extends State<ShoppingInfo> {
  final Stream<QuerySnapshot> _genaralStream =
      FirebaseFirestore.instance.collection('shopping').snapshots();
  CollectionReference users = FirebaseFirestore.instance.collection('shopping');
  String VThai = "";
  String Vocabulary = "";
  int shopId=0 ;
  CollectionReference favorite =
      FirebaseFirestore.instance.collection('favorite');
  Future<void> addFavorite() {
    return favorite
        .add({
          'id':this.shopId,
          'thai': this.VThai,
          'vocabulary': this.Vocabulary,
        })
        .then((value) => print('Favorite Add'))
        .catchError((error) => print("Failed to add favorite: $error"));
  }
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _genaralStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text('Loading');
        }
        return Scaffold(
          appBar: AppBar(
            title: Text('Shopping'),
          ),
          body: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [Colors.grey.shade900, Colors.grey.shade800])),
              padding: EdgeInsets.all(16.0),
              child: ListView(
                children: snapshot.data!.docs.map((DocumentSnapshot document) {
                  Map<String, dynamic> data =
                      document.data()! as Map<String, dynamic>;
                  return Card(
                    color: Colors.grey.shade700,
                    child: ListTile(
                      title: Text(data['thai']),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                              onPressed: () {
                                setState(() {
                                  shopId = data['id'];
                                VThai = data['thai'];
                                Vocabulary = data['vocabulary'];
                                
                                addFavorite();
                                
                                });
                              },
                              icon: Icon(Icons.star)),
                        ],
                      ),
                      onTap: () {
                         shopId = data['id'];
                                VThai = data['thai'];
                                Vocabulary = data['vocabulary'];
                                Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ShopVocab(
                                      Sid: shopId,
                                      Vthai: VThai,
                                      Vocab: Vocabulary,
                                    )));
                      },
                    ),
                  );
                }).toList(),
              )),
        );
      },
    );
  }
}